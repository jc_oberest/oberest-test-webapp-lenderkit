/* NOTE! This file contains special comments fot autosetup.
  If you add any optional module to webapp application, please surround all the module lines with the next comments
  //module-{same-as-appropriate-backend-module-name}-lines-begin fot lines start
  //module-{same-as-appropriate-backend-module-name}-lines-end fot lines finish
  Note, that modulec-module comments has NO space after //
*/






//module-donation-lines-begin
import Donation from '@donation/Installer';
//module-donation-lines-end

//module-debt-lines-begin
import Debt from '@debt/Installer';
//module-debt-lines-end


export const enabledModules = [








//module-donation-lines-begin
  'donation',
//module-donation-lines-end
//module-debt-lines-begin
  'debt',
//module-debt-lines-end

];

export default [





//module-donation-lines-begin
  Donation,
//module-donation-lines-end

//module-debt-lines-begin
  Debt,
//module-debt-lines-end

];
