# Makefiles reference

As we already mentioned there are 3 Makefiles:

* `/Makefile` or **root level Makefile**.
This file contains targets to configure your server environment and operate with Docker containers.
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant SSH).
* `/app/Makefile` or **application level Makefile**.
This file contains targets to operate with your JavaScript application.
It should be called inside Node containers (webapp-app).
/app folder is mounted as a docroot /var/www/html to docker node.js containers, so it's the only available Makefile inside the containers.
In general this Makefile contains application install/update scripts calls.
* `/api/Makefile` or **WebApp API level Makefile**.
This file contains targets to operate with WebApp API Server.


## Root level Makefile

### Docker Targets

**info:**
Print base available commands.

**init:**
Initialize main config file like `.env`, `docker-compose.yml`, `nginx-server.conf`.

**init-dev:** _depends on `init`_
Initialize for development. Create `docker-compose.override.yml` file.

**init-rsa:**
Copy the 'id_rsa' file to the project to mount inside the docker that's because `npm` should have access to the private repositories

**install:**
Runs full installation process in required containers:
* run `make install`
* run npm install/npm build

**update:**
Runs full update process
* run `make update`
* run npm install/npm build if not `SKIP_VENDORS` flag specified

**run:**
Runs the application

**test-run:**
Runs the application for the test

**stop:**
Stop containers and remove docker network

**chown:**
Fix file permissions for files created inside docker container (**should be run outside the container or Vagrant**)

### Node.js Related Targets

**nodejs-bash:**
Login to the Node.js container

**api-bash:**
Login to the Node.js API-server bash

The following helpers are used in the `webapp-nginx` and `webapp-app` containers.

**npm-build:**
Runs `npm run build` with node memory limit option.

**npm-multisite-build:**
Builds the multisite

**npm-watch:**
Runs build in watch mode with node memory limit option (`npm run watch`)


## Application level Makefile

### Docker Targets

**info:**
Print base available commands.

**install:**
Install dependencies, init core library

**update:**
Update after pull

**chmod:**
Chmod important folders

**dotenv-init:**
Create .env file

**themeconfig-init:**
Create theme.config.js

**gtmconfig-init:**
Create gtm.config.js

**packagejson-init:**
Init package.json

**corelibrary-init:**
Init the core library. It needs to develop the LenderKit core to replace
downloaded modules from the private repositories with local copies of the modules
with usage of the symlink.


## WebApp API level Makefile

### Docker Targets

**devmode-init:**
Init dev mode (replaces downloaded `lenderkit-webapp-api` module with local copy)

**dotenv-init:**
Create .env

**install:**
Install the Web API Server

**update:**
Update the Web API Server
