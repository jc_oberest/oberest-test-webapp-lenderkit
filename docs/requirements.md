# Requirements

We assume that you read [Before you start](before-you-start.md) section and aknowledged about what Docker, Vagrant 
and Makefile are.

## Host requirements

You can install this project on Linux or MacOS machine. Once you reached Host requirements you will be ready to
start installation process with Make util and Docker.

### Linux

#### Docker Setup

* To install Docker CE you can follow Official documentation: [Docker Community Edition installation for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* Your user should be added to `docker` group. Do not run docker as superuser! ([Executing Docker without Sudo](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04#step-2-%E2%80%94-executing-the-docker-command-without-sudo-optional))

#### Docker Compose

* To upgrade docker-compose utility you can follow [Digital Ocean instructions](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04). 

#### Make util

* Make sure you have `make` util installed

```bash
make -h
```

### MacOS

If you want to install project on MacOS, then you will need to install some additional software:

* **XCode and XCode CLI** - Provide additional command line tools and required by Homebrew
* **Homebrew** - CLI software package manager
* **Virtual Box**
* **Vagrant**

#### XCode

* Download XCode from App Store (it will take time)
* [Install XCode Command Line Tools](https://railsapps.github.io/xcode-command-line-tools.html)

#### Homebrew

* Install [Homebrew](https://brew.sh/)

#### Vagrant 

* Install [Vagrant and Virtual Box with Homebrew](https://medium.com/@JohnFoderaro/macos-sierra-vagrant-quick-start-guide-2b8b78913be3)  
* Place `Vagrantfile` from **[this snippet](https://bitbucket.org/snippets/justcoded/n7zpkB)** to a folder with all your projects.  
Read snippet Readme on how to run Vagrant.
* ALL commands for installation process should be run inside Vagrant SSH 
(except very little amount of commands, which are marked to NOT be executed under Vagrant) 

#### Make util

* Check that you have `make` util available (it should be available after XCode CLI installed)

```bash
make -h
```
